
function defaults(obj, defaultProps) {
    // Fills the undefined properties that match properties on the `defaultProps` parameter object and returns `obj`.
    if (obj === null || typeof obj != 'object' || Array.isArray(obj) == true || defaultProps === null || typeof defaultProps != 'object' || Array.isArray(defaultProps) == true) {
        return null;
    }

    const newObj = {...obj};
    let exists = (prop)=>{
        let found = false;
        for (let key in obj){
            if (prop == key){
                found = true;
                break;
            }
        }
        return found;
    };

    for(let prop in defaultProps){
        if(exists(prop) == false){
            newObj[prop] = defaultProps[prop];
        }

    }
    return newObj;
}

module.exports = defaults;

