
function values(obj) {
    // Returns all of the values of the object's own properties, ignoring functions
    if (obj === null || typeof obj != 'object' || Array.isArray(obj) == true) {
        return null;
    }

    const objValues = [];
    let value;

    for (const key in obj) {
        value = obj[key.toString()];
        if (typeof value != 'function') {
            objValues.push(value);
        }
    }
    return objValues;

}

module.exports = values;

