
function mapObject(obj,cb) {
   //Transforms the value of each property by passing it to the callback function.
    if (obj === null || typeof obj != 'object' || Array.isArray(obj) == true || typeof cb != 'function') {
        return null;
    }

    const resultObject = {};
    let newVal;

    for(let key in obj){
        newVal = cb(obj[key]);
        resultObject[key] = newVal;
    }
    return resultObject;
}


module.exports = mapObject;
