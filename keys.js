
function keys(obj) {
    // Retrieves all the names of the object's properties.
    // Returns the keys as strings in an array.
    if (obj === null || typeof obj != 'object') {
        return null;
    }

    const objKeys = [];

    if (Array.isArray(obj) == true) {
        for (let index = 0; index < obj.length; index++) {
            if (obj[index] !== undefined) {
                objKeys.push(index.toString());
            }
        }

        return objKeys;
    }

    for (const key in obj) {
        objKeys.push(key.toString());
    }
    return objKeys;

}

module.exports = keys;