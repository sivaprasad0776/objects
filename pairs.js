
function pairs(obj) {
   //Converts an object into a list of [key, value] pairs.
    if (obj === null || typeof obj != 'object' || Array.isArray(obj) == true) {
        return null;
    }

    const result = [];
    
    for(let key in obj){
        const pair = [];
        pair[0] = key.toString();
        pair[1] = obj[key];
        result.push(pair);
    }
    return result;
}


module.exports = pairs;
