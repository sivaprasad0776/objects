
function invert(obj) {
   //Converts an object into a list of [key, value] pairs.
    if (obj === null || typeof obj != 'object' || Array.isArray(obj) == true) {
        return null;
    }

    const result = {};
    
    for(let key in obj){
        const val = obj[key].toString();
        result[val] = key;
    }
    return result;
}


module.exports = invert;
