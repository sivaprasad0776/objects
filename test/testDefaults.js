const testObject = require('../objects');
const defaults = require('../defaults');

const defaultProps = {
    name: 'Siva Prasad', 
    age: 36,
    qualification : 'B.Tech'
};
const result = defaults(testObject, defaultProps);
console.log(result);
