const mapObject = require('../mapObject');
const testObject = {
    one : 1,
    two : 2,
    three : 3
};

const cb = function(val){
    let doubleValue = val *2;
    return doubleValue;
}

const result = mapObject(testObject, cb);
console.log(result);
